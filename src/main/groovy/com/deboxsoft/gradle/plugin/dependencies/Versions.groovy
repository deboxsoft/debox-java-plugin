/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-java-plugin
 *  Module     : debox-java-plugin
 *  File       : Versions.groovy
 *  ClassName  : Versions.groovy
 *  Modified   : 15038410
 */

package com.deboxsoft.gradle.plugin.dependencies


class Versions {
    public static Properties getProperties() {
        Properties properties = new Properties()
        properties.setProperty('deboxpluginVersion', '3.0-SNAPSHOT')
        properties.setProperty('deboxCoreParentVersion', '1.1.0-SNAPSHOT')
        properties.setProperty('springJade4jVersion', '1.1.4')
        properties.setProperty('bonecpVersion', '0.8.0.RELEASE')
        properties.setProperty('guavaVersion', '18.0')
        properties.setProperty('commonsIOVersion', '2.4')
        properties.setProperty('querydslVersion', '3.3.1')
        properties.setProperty('apacheFelixVersion', '3.0.2')
        properties.setProperty('atmosphereVersion', '2.2.4')
        properties.setProperty('lombokVersion', '1.16.2')
        properties.setProperty('findbugsVersion', '3.0.1')
        properties
    }
}
