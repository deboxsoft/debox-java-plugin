/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-java-plugin
 *  Module     : debox-java-plugin
 *  File       : Library.groovy
 *  ClassName  : Library.groovy
 *  Modified   : 15038410
 */

package com.deboxsoft.gradle.plugin.dependencies

import org.gradle.api.Project

class Library {


    private static def getLibs(Properties properties) {
        return [
                // ***************** Internal library **************
                deboxCoreApi                 : "com.deboxsoft.core:debox-core-api",
                deboxCoreUtils               : "com.deboxsoft.core:debox-core-utils",
                deboxCoreCache               : "com.deboxsoft.core:debox-core-cache",
                deboxCoreEvent               : "com.deboxsoft.core:debox-core-event",
                deboxCoreConfig              : "com.deboxsoft.core:debox-core-config",
                deboxCoreServlet             : "com.deboxsoft.core:debox-core-servlet",
                deboxCoreParent              : "com.deboxsoft.core:debox-core-parent:${properties.getProperty('deboxCoreParentVersion')}",

                // plugin
                deboxpluginCore              : "com.deboxsoft.plugin:dboxplugin-core:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginMain              : "com.deboxsoft.plugin:dboxplugin-main:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginOsgi              : "com.deboxsoft.plugin:dboxplugin-osgi:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginOsgiBridge        : "com.deboxsoft.plugin:dboxplugin-osgi-bridge:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginOsgiEvent         : "com.deboxsoft.plugin:dboxplugin-osgi-event:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginOsgiSpringExtender: "com.deboxsoft.plugin:dboxplugin-osgi-spring-extender:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginSpring            : "com.deboxsoft.plugin:dboxplugin-spring:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginWebAssets         : "com.deboxsoft.plugin:dboxplugin-web-assets:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginWebCore           : "com.deboxsoft.plugin:dboxplugin-web-core:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginWebFragment       : "com.deboxsoft.plugin:dboxplugin-web-fragment:${properties.getProperty('deboxpluginVersion')}",
                deboxpluginTest              : "com.deboxsoft.plugin:plugintest:${properties.getProperty('deboxpluginVersion')}",

                // ********************* library core **************
                // spring framework
                springCore                   : "org.springframework:spring-core",
                springContext                : "org.springframework:spring-context",
                springContextSupport         : "org.springframework:spring-context-support",
                springBeans                  : "org.springframework:spring-beans",
                springOrm                    : "org.springframework:spring-orm",
                springJdbc                   : "org.springframework:spring-jdbc",
                springExpression             : "org.springframework:spring-expression",
                springWebsocket              : "org.springframework:spring-websocket",

                // Tools
                guava                        : "com.google.guava:guava:${properties.getProperty('guavaVersion')}",
                commonsCollections           : "commons-collections:commons-collections:3.2.1",
                commonsIo                    : "commons-io:commons-io:${properties.getProperty('commonsIOVersion')}",
                commonsLang3                 : "org.apache.commons:commons-lang3:3.0.1",
                lombok                       : "org.projectlombok:lombok:${properties.getProperty('lombokVersion')}",

                // joda time
                jodaTime                     : "joda-time:joda-time",

                // xml
                dom4j                        : "dom4j:dom4j:${properties.getProperty('dom4jVersion')}",
                classworlds                  : "classworlds:classworlds:1.1",

                // database
                deboxDbHibernate             : "com.deboxsoft.db:debox-db-hibernate",
                deboxDbMybatis               : "com.deboxsoft.db:debox-db-mybatis",
                deboxDbNeo4j                 : "com.deboxsoft.db:debox-db-neo4j",
                hibernateCore                : "org.hibernate:hibernate-core",
                hibernateEntitymanager       : "org.hibernate:hibernate-entitymanager",
                hibernateEhcache             : "org.hibernate:hibernate-ehcache",
                hibernateValidator           : "org.hibernate:hibernate-validator",
                springDataJpa                : "org.springframework.data:spring-data-jpa",
                springDataNeo4j              : "org.springframework.data:spring-data-neo4j",
                querydslApt                  : "com.mysema.querydsl:querydsl-apt:${properties.getProperty('querydslVersion')}",
                querydslJpa                  : "com.mysema.querydsl:querydsl-jpa:${properties.getProperty('querydslVersion')}",
                querydslCodegen              : "com.mysema.querydsl:querydsl-codegen:${properties.getProperty('querydslVersion')}",
                liquibaseCore                : "org.liquibase:liquibase-core",
                bonecp                       : "com.jolbox:bonecp:${properties.getProperty('bonecpVersion')}",
                bonecpSpring                 : "com.jolbox:bonecp-spring:${properties.getProperty('bonecpVersion')}",

                // cache
                ehcacheCore                  : "net.sf.ehcache:ehcache-core",

                // log
                logbackCore                  : "ch.qos.logback:logback-core",
                logbackClassic               : "ch.qos.logback:logback-classic",
                jclOverSlf4j                 : "org.slf4j:jcl-over-slf4j",

                // ********************* library spesifik **************

                // embed compiler
                janino                       : "org.codehaus.janino:janino",
                commonsCompilerJdk           : "org.codehaus.janino:commons-compiler-jdk",

                // web
                springWeb                    : "org.springframework:spring-web",
                springWebmvc                 : "org.springframework:spring-webmvc",
                servletApi                   : "javax.servlet:javax.servlet-api",
                velocity                     : "org.apache.velocity:velocity",
                springJade4j                 : "de.neuland-bfi:spring-jade4j:${properties.getProperty('springJade4jVersion')}",

                //communication
                atmosphereRuntime            : "org.atmosphere:atmosphere-runtime:${properties.getProperty('atmosphereVersion')}",

                // spring-security
                springSecurityCore           : "org.springframework.security:spring-security-core",
                springSecurityWeb            : "org.springframework.security:spring-security-web",
                springSecurityConfig         : "org.springframework.security:spring-security-configuration",
                springSecurityAcl            : "org.springframework.security:spring-security-acl",
                springSecurityLdap           : "org.springframework.security:spring-security-ldap",
                springSecurityOpenid         : "org.springframework.security:spring-security-openid",
                springSecurityCas            : "org.springframework.security:spring-security-cas",
                springSecurityCrypto         : "org.springframework.security:spring-security-crypto",

                // spring boot
                springBoot                   : "org.springframework.boot:spring-boot",
                springBootAutoconfigure      : "org.springframework.boot:spring-boot-autoconfigure",
                springBootStarter            : "org.springframework.boot:spring-boot-starter",
                springBootStarterWeb         : "org.springframework.boot:spring-boot-starter-web",
                springBootStarterActuator    : "org.springframework.boot:spring-boot-starter-actuator",
                springBootStarterTomcat      : "org.springframework.boot:spring-boot-starter-tomcat",
                springBootStarterSecurity    : "org.springframework.boot:spring-boot-starter-security",
                springBootStarterTest        : "org.springframework.boot:spring-boot-starter-test",
                springBootDevtools           : "org.springframework.boot:spring-boot-devtools",

                // groovy
                groovyAll                    : "org.codehaus.groovy:groovy-all",

                // Driver
                h2                           : "com.h2database:h2",
                mysqlConnectorJava           : "mysql:mysql-connector-java",

                // testing
                deboxCoreTest                : "com.deboxsoft.core:debox-core-test",
                springTest                   : "org.springframework:spring-test",

                // findbug
                findbugs                     : "com.google.code.findbugs:findbugs:${properties.getProperty('findbugsVersion')}",
        ]
    }

    public static void configJava(Project project, String location) {
        Properties properties = new Properties(Versions.properties)
        File projectBaseLocation = project.file(location)
        File rootBaseLocation = project.file(project.rootDir.path + location)
        if (projectBaseLocation.isFile())
            properties.load(new FileReader(projectBaseLocation))
        else if (rootBaseLocation.isFile())
            properties.load(new FileReader(rootBaseLocation))
        project.ext.libs = getLibs(properties)
    }
}
