/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-java-plugin
 *  Module     : debox-java-plugin
 *  File       : DeboxJavaPlugin.groovy
 *  ClassName  : DeboxJavaPlugin.groovy
 *  Modified   : 15038410
 */

package com.deboxsoft.gradle.plugin

import com.deboxsoft.gradle.plugin.configuration.Idea
import com.deboxsoft.gradle.plugin.dependencies.Library
import nebula.plugin.extraconfigurations.OptionalBasePlugin
import nebula.plugin.extraconfigurations.ProvidedBasePlugin
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginConvention

class DeboxJavaPlugin implements Plugin<Project> {


    public static final String SOURCE_COMPATIBILITY = "sourceCompatibility"
    public static final String TARGET_COMPATIBILITY = "targetCompatibility"

    void apply(Project project) {
        project.plugins.apply(JavaPlugin)
        project.plugins.withType(JavaPlugin) { JavaPlugin javaPlugin ->
            javaProject(project)
        }
    }


    public static void javaProject(Project project) {
        Idea.loadConfig(project)
        project.plugins.apply(OptionalBasePlugin)
        project.plugins.apply(ProvidedBasePlugin)
        JavaPluginConvention javaPluginConvention = project.convention.getPlugin(JavaPluginConvention)
        if (project.hasProperty(SOURCE_COMPATIBILITY))
            javaPluginConvention.sourceCompatibility = JavaVersion.toVersion(project.property(SOURCE_COMPATIBILITY))
        else
            javaPluginConvention.sourceCompatibility = JavaVersion.VERSION_1_8
        if (project.hasProperty(TARGET_COMPATIBILITY))
            javaPluginConvention.targetCompatibility = JavaVersion.toVersion(project.property(TARGET_COMPATIBILITY))
        else
            javaPluginConvention.targetCompatibility = JavaVersion.VERSION_1_8
        Library.configJava(project, 'dependencies-version.properties')

    }

}
