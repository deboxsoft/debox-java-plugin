/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-java-plugin
 *  Module     : debox-java-plugin
 *  File       : Idea.groovy
 *  ClassName  : Idea.groovy
 *  Modified   : 15038410
 */

package com.deboxsoft.gradle.plugin.configuration

import org.gradle.api.Project
import org.gradle.plugins.ide.idea.IdeaPlugin

class Idea {

    public static loadConfig(Project project) {
        project.plugins.apply(IdeaPlugin)
        project.idea.module {
            //  configuration output compile
            inheritOutputDirs = false
            outputDir = project.file("${project.buildDir}/classes/main")
            testOutputDir = project.file("${project.buildDir}/classes/test")
        }
    }
}
