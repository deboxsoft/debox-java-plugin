/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-java-plugin
 *  Module     : debox-java-plugin
 *  File       : DeboxJavaPluginTest.groovy
 *  ClassName  : DeboxJavaPluginTest.groovy
 *  Modified   : 15038606
 */

package com.deboxsoft.gradle.plugin

import nebula.test.PluginProjectSpec


class DeboxJavaPluginTest extends PluginProjectSpec {
    String getPluginName() {
        return "debox-java"
    }

    def "apply"() {
        when:
        project.plugins.apply(DeboxJavaPlugin)
        then:
        project.ext.libs != null
    }
}
