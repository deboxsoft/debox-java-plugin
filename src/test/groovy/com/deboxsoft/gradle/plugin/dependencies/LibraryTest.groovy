/*
 * Copyright (c) 2015. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : debox-java-plugin
 *  Module     : debox-java-plugin
 *  File       : LibraryTest.groovy
 *  ClassName  : LibraryTest.groovy
 *  Modified   : 15038602
 */

package com.deboxsoft.gradle.plugin.dependencies

import nebula.test.ProjectSpec


class LibraryTest extends ProjectSpec {

    def "loading properties from file"() {
        when:
        Library.configJava(project, '../../../resources/test/test.properties')
        def libs = project.ext.libs
        def properties = Versions.properties

        then:
        libs.size() > 0
        libs.deboxcoreApi == "com.deboxsoft.core:dboxcore-api:1.test.version"
        libs.deboxpluginCore == "com.deboxsoft.plugin:dboxplugin-core:${properties.getProperty('deboxpluginVersion')}"
    }

    def "cek kalau tidak ada file"() {
        when:
        Library.configJava(project, "fileNotFound.properties")
        then:
        true
    }
}
